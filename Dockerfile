FROM arzzen/git-quick-stats

RUN apk --update add --no-cache nodejs npm
RUN npm install -g node-fetch

COPY analyze.js /usr/local/bin/analyze
RUN chmod +x /usr/local/bin/analyze

#ENV NODE_PATH=/usr/local/lib/node_modules
ENV NODE_PATH=/usr/lib/node_modules

CMD ["/bin/sh"]
