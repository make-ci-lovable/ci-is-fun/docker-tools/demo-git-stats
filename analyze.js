#!/usr/bin/env node

const fs = require("fs")
const fetch = require('node-fetch')

// git-quick-stats --detailed-git-stats > detailed-git-stats.txt
let report = fs.readFileSync("./detailed-git-stats.txt", "utf8")

let lines = report.split("\n\t").map(item => item.trim())
let totalPosition = lines.indexOf("total:")
let jsonStats = lines.slice(totalPosition+1).map(item => {
  let label = item.split(":")[0]
  let value = item.split("\t")[0].split(":")[1].trim()
  return {label, value}
})

let reportHeader = [
  "### Git stats report",
  "| actions | values |", 
  "| ------- | ------ |"
]

let reportRows = jsonStats.map(item => {
  if(item.label=="insertions") item.label = "🟢 " + item.label
  if(item.label=="deletions") item.label = "🔴 " + item.label
  if(item.label=="files") item.label = "📝 " + item.label
  if(item.label=="commits") item.label = "✅ " + item.label
  return `| ${item.label} | ${item.value}| `
})

let mermaidHeader = [
  "```mermaid",
  "pie title Git stats"
]

let mermaidRows = jsonStats.map(item => {
  return `    "${item.label}" : ${item.value}`
})
mermaidRows.push("```")
let mermaidReport = mermaidHeader.concat(mermaidRows)

let stats = reportHeader.concat(reportRows)

// git-quick-stats --suggest-reviewers > suggest-reviewers.txt
let reviewers = fs.readFileSync("./suggest-reviewers.txt", "utf8")
                  .split("\n").map(item => item.trim())
                  .slice(2)
                  .filter(item => item != "")
                  .map((item, index) => `| 👀 reviewer_${index+1} | ${item.split(" ").slice(1).join(" ")} |`)

let gitReport = stats.concat(reviewers).concat(mermaidReport).join("\n")

console.log(gitReport)


let CI_MERGE_REQUEST_PROJECT_ID = process.env.CI_MERGE_REQUEST_PROJECT_ID
let CI_MERGE_REQUEST_IID = process.env.CI_MERGE_REQUEST_IID
let CI_API_V4_URL = process.env.CI_API_V4_URL

let headers = {
  "Content-Type": "application/json",
  "Private-Token": process.env.BOT_TOKEN
}

let path = `/projects/${CI_MERGE_REQUEST_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/notes`
return fetch(`${CI_API_V4_URL}/${path}`, {
  method: 'POST',
  body: JSON.stringify({body:gitReport}),
  headers: headers
})
.then(res => res.json())
.then(json => {
  console.log("🦊 return from GitLab", json)
})
.catch(error => {
  return error
})


